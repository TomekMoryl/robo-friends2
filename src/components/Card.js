import React from 'react';

const Card = (props) => {
    const {id, name, email} = props;
    return (
        <div className='bg-light-pink dib br3 pa3 ma2 grow bw2 shadow-3'>
            <img src ={`https://robohash.org/478${id}512?200x200`} alt='robo photo' />
            <div>
                <h2>{name}</h2>
                <p>{email}</p>
            </div>
        </div>
    )
};

export default Card;
