<div style="display: none">

- [Overview](#overview)
- [Obtaining a Token](#obtaining-a-token)
  - [Entry Point Resource](#entry-point-resource)
  - [Identity Providers](#identity-providers)
- [Obtaining a Token. Resource Owner Password Credentials Grant](#obtaining_token_ropc)
  - [Example](#example_ropc)
- [Obtaining a Token via refresh token. Resource Owner Password Credentials Grant](#refreshing_token_rop)
  - [Example](#example_ropc_refreshing)
- [Obtaining a Token. Client Credentials Grant](#obtaining_token_cc)
  - [Example](#example_cc)
- [Client errors during ROPC an CC workflows](#errors)
- [Using PingFederate as Identity Provider](#using-ping-federate-identity-provider)
- [Configuring Certificate for SAML Response Signature Validation](#configuring-saml-certificate)
- [Passing and Verifying Access Token](#passing-and-verifying-access-token)
- [Obtaining Current Token](#obtaining-current-token)
- [Logging out using Current Token link](#loggging-out-current-token)
- [Extending current token](#extending-current-token)

</div>
